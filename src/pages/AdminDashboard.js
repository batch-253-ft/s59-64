import { Container, Tab, Tabs, Row, Col, Image } from 'react-bootstrap';

import AllProducts from '../components/productsComponents/AllProducts';
import Carts from './Carts';
import CreateProduct from '../components/productsComponents/CreateProduct';

import Users from '../components/userComponents/Users';


export default function AdminDashboard() {


    return (
        <Container>
            <h1 className='text-center m-3 font-main-header'>Welcome Admin</h1>
            <Container fluid>
                <Row>
                    <Col>
                    <Image fluid src='https://res.cloudinary.com/dlk7w16gw/image/upload/v1679833155/cxfqgkayhff7iveamplp.png'/>
                    </Col>
                    <Col>
                    <Image fluid src='https://res.cloudinary.com/dlk7w16gw/image/upload/v1679677836/t5rx5pc3g0pl3eapijk0.png'/>
                    </Col>
                    <Col>
                    <Image fluid src='https://res.cloudinary.com/dlk7w16gw/image/upload/v1679677835/phy4fpkzn35qhynewiux.png'/>  
                    </Col>
                    
                </Row>
            </Container>
            <Tabs
                defaultActiveKey="createProduct"
                id="justify-tab-example"
                className="mx-5"
                justify>
                    <Tab className='mx-5' eventKey="createProduct" title="Create Product">
                        <CreateProduct />
                    </Tab>
                    <Tab className='mx-5' eventKey="allProducts" title="All Products">
                        <AllProducts />
                    </Tab>
                    <Tab className='mx-5' eventKey="showUserOrders" title="Show User Orders" >
                        <Carts />
                    </Tab>
                    <Tab className='mx-5' eventKey="showUserDetails" title="Show User Details" >
                        <Users />
                    </Tab>
            </Tabs>
        </Container>
    )
}