import { Card, Row, Col, Container, CardGroup } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';

export default function ProductCard({ product }) {

    // Deconstruct the course properties into their own variables
    const { _id, productName, description, price, imageUrl } = product;
    const navigate = useNavigate()

    return (          
        <Card style={{
            height: '26rem',
            width: '17rem'
        }} className='forms'>
            <Card.Img style={{
                height: '12rem',
                objectFit: 'cover'
                }} className='image-fluid' variant="top"src={imageUrl} />
            <Card.Body>
                <Card.Title className='text-center font-text'>{productName}</Card.Title>
                <Card.Subtitle className='font-text'>Description:</Card.Subtitle>
                <Card.Text className='font-deets'>{description}</Card.Text>
                
            </Card.Body>
            <Card.Footer>
                <Row>
                    <Col >
                        <Card.Subtitle className='font-text'>Price:</Card.Subtitle>
                        <Card.Text className='font-text'><strong> ₱{parseFloat(price).toLocaleString()}</strong></Card.Text>
                        
                    </Col>
                    <Col className='d-grid '>
                        <Link className="btn btn-secondary font-button1 " to={`/products/${_id}`}>Details</Link>
                    </Col>
                </Row>
            </Card.Footer>
        </Card>
    )
}