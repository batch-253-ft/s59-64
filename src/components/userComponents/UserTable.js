import { useEffect, useState } from "react";
import { Link, useNavigate } from "react-router-dom";

export default function UserTable({ user }){

    const {_id, firstName, lastName, mobileNum, email, isAdmin } = user;

    const navigate = useNavigate();

    // function toAdmin(){
    //     fetch(`http://localhost:4000/users/${_id}/updateUserRole`, {
    //         method: 'PATCH',
    //         headers: {
    //             'Content-Type': 'application/json',
    //             "Authorization": `Bearer ${localStorage.getItem('token')}`
    //         },
    //         body: JSON.stringify({
    //             userId: _id,
    //             isAdmin: true
    //         })
    //     })
    //     .then(res => res.json())
    //     .then(data => {
    //         console.log(data)
    //     })
    // }

    // function toUser(_id){
    //     fetch(`http://localhost:4000/users/${_id}/updateUserRole`, {
    //         method: 'PATCH',
    //         headers: {
    //             'Content-Type': 'application/json',
    //             "Authorization": `Bearer ${localStorage.getItem('token')}`
    //         },
    //         body: JSON.stringify({
    //             userId: _id,
    //             isAdmin: false
    //         })
    //     })
    //     .then(res => res.json())
    //     .then(data => {
    //         console.log(data)
    //     })
    // }


    useEffect(() => {
        navigate('/adminDashboard');
 
    }, [ isAdmin ])

    return(
        <>
            <td>{ _id }</td>
            <td>{ firstName} {lastName} </td>
            <td>{ mobileNum }</td>
            <td>{ email }</td>
            <td>{ isAdmin ? "Admin" : "User" }</td>
          </>

    )
}